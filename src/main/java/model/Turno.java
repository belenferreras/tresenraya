package model;

public class Turno {
    private Ficha ficha;//Se llama a la CLASE Ficha con la constante ficha (X o 0)

    public Turno(Ficha ficha) { //Descripción del metodo Turno para acceder a la clase Ficha con la constatne ficha
        this.ficha = ficha; //Metodo Turno(CLASE CONSTANTE){esta CONSTANTE = ficha}
    }

    public void cambiarTurno() { //Descripción del metodo cambiarTurno
        if (this.ficha.equals(Ficha.X)) //Si la constante que se refiere a la clase Ficha es X
            this.ficha = Ficha.O; //Entonces la constante ficha cambia a 0
        else
            this.ficha = Ficha.X; // Si no la constatne sigue siendo X
    }

    public Ficha getFicha() { //Descripción del metodo getFicha de la CLASE Ficha
        return ficha; //Devuelve el valor de ficha (X o 0)
    }

    @Override
    public String toString() { //Descripcion del metodo toString para convertir en texto el objeto ficha
        return this.ficha.toString(); //Devuelve el objeto ficha convertido a texto
    }
}
