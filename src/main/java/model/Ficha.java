package model;
//Representación de los jugadores X y 0
/*enum: CLASE especial que representa un grupo de constantes
**Para acceder a las constantes Ficha ficha = Ficha.X o Ficha.0; */
public enum Ficha {
    X, O
}
