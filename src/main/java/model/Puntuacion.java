package model;

public class Puntuacion {
    private int ganaX; //Se decalara variable privada (solo se puede acceder a ello dentro de la CLASE Puntuación)
    private int ganaO;

    public Puntuacion() { //Metodo dentro de la CLASE Puntuacion en el que se declara esas variables con valor nulo
        this.ganaX = 0;
        this.ganaO = 0;
    }
/* VOID indica que el metodo no devuelve ningun valor */
    public void ganaX(){ //Fuera de la CLASE Puntuacion se describe el metodo ganaX
        this.ganaX++; //En el que la variable ganaX de la CLASE Puntuacion se incrementa a 1
    }
    public void ganaO(){ //Descripción del metodo gana0
        this.ganaO++; //En el que la variable gana0 de la CLASE Puntuacion se incrementa a 1
    }
    public int getGanaX() { //Descripción del metodo getGanaX que será de tipo entero
        return ganaX; //En el que se devuelve el valor de ganaX de la CLASE Puntuacion
    }
    public int getGanaO() { //Descripción del metodo getGana0 que será de tipo entero
        return ganaO; //En el que se devuelve el valor de gana0 de la CLASE Puntuacion
    }
    public void reset() { //Descripcion del metodo reset (No devuelve ningun valor tipo void)
        this.ganaX = 0; //En la que se actualiza la variable ganaX de la CLASE Puntuacion a 0
        this.ganaO = 0; //En la que se actualiza la variable gana0 de la CLASE Puntuacion a 0
    }
}
