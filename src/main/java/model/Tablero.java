package model;

import java.util.Arrays;

public class Tablero {

    private static final int NUM_POSICIONES_TABLERO = 9; //Se declara variable que no puede cambiarse
    private static final int NUM_COMBINACIONES_GANADORAS = 7; //Se declara variable que no puede cambiarse

    /*Se declara array posiciones que tendrá una fila con la longitud de la variable NUM_POSCIONES_TABLERO (9)*/
    private String[] posiciones = new String[NUM_POSICIONES_TABLERO];

    public Tablero() { //Descripcíón del metodo Tablero
        //Se recorre el array para leer todas las posiciones mientras estas no superen 9
        for (int i = 0; i < NUM_POSICIONES_TABLERO; i++) {
            posiciones[i] = String.valueOf(i + 1); //Como i empieza en 0 para actualizar el valor de posiciones se le suma 1
            /*Se actualiza el valor de posiciones */
        }
    }

    public String getPosicion(int posicion) {
        return this.posiciones[posicion - 1];
    }

    public void ponerFicha(int posicion, Ficha ficha) {
        this.posiciones[posicion - 1] = ficha.name();
    }

    public boolean posicionLibre(int posicion) {
        return this.posiciones[posicion - 1].equals(String.valueOf(posicion));
    }

    public boolean estaLleno() {
        for (int i = 0; i < NUM_POSICIONES_TABLERO; i++) {
            if (Arrays.asList(this.posiciones).contains(String.valueOf(i + 1))) return false;
        }
        return true;
    }

    public boolean hayGanador() {
        String raya = "";

        for (int i = 0; i <= NUM_COMBINACIONES_GANADORAS; i++) {//i debe ser menor o igual a 7 sino nunca llegará a la opcion 7 del switch
            switch (i) {
                case 0:
                    raya = this.posiciones[0] + this.posiciones[1] + this.posiciones[2];
                    break;
                case 1:
                    raya = this.posiciones[3] + this.posiciones[4] + this.posiciones[5];
                    break;
                case 2:
                    raya = this.posiciones[6] + this.posiciones[7] + this.posiciones[8];
                    break;
                case 3:
                    raya = this.posiciones[0] + this.posiciones[3] + this.posiciones[6];
                    break;
                case 4:
                    raya = this.posiciones[1] + this.posiciones[4] + this.posiciones[7];
                    break;
                case 5:
                    raya = this.posiciones[2] + this.posiciones[5] + this.posiciones[8];
                    break;
                case 6:
                    raya = this.posiciones[0] + this.posiciones[4] + this.posiciones[8];
                    break;
                case 7:
                    raya = this.posiciones[2] + this.posiciones[4] + this.posiciones[6];
                    break;
            }

            if (raya.equals("XXX") || raya.equals("OOO")) return true;
        }
        return false;
    }

}
